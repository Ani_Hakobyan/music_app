package ani.am.music.classes;

import android.media.MediaPlayer;

import java.io.Serializable;

public class Song implements Serializable {
    private String authorName;
    private String songName;
    private int img;
    private int player;

    public Song() {
    }

    public Song(String authorName, String songName, int img, int player) {
        this.authorName = authorName;
        this.songName = songName;
        this.img = img;
        this.player = player;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }
}
