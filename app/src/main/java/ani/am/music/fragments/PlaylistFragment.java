package ani.am.music.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ani.am.music.R;
import ani.am.music.adapters.RecyclerViewAdapter;
import ani.am.music.classes.Song;

public class PlaylistFragment extends Fragment {
    private List<Song> playlist ;
    private RecyclerView resView;

    public PlaylistFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v;
        v =inflater.inflate(R.layout.fragment_playlist,container,false);
        resView = (RecyclerView)v.findViewById(R.id.recyclerView_ID);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(getContext(),playlist);
        resView.setLayoutManager(new LinearLayoutManager(getContext()));
        resView.setAdapter(adapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playlist = new ArrayList<>();
        playlist.add(new Song("Theme From Love Story","piano",R.drawable.musician,R.raw.theme_from_love_story_piano ));
        playlist.add(new Song("Mger Armenia & Roza Filberg","Harazat Hogi",R.drawable.mgerroza,R.raw.harazat_hogi ));
    }
}
