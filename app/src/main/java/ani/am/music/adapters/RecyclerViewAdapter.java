package ani.am.music.adapters;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;

import ani.am.music.R;
import ani.am.music.activites.SelectedSongActivity;
import ani.am.music.classes.Song;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>
{
    private Context context;
    private List<Song> mdata;

    public RecyclerViewAdapter(Context context, List<Song> mdata) {
        this.context = context;
        this.mdata = mdata;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int position) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.item_playlist,parent,false);
        final MyViewHolder myViewHolder = new MyViewHolder(v);

        myViewHolder.itemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "selected "+myViewHolder.getAdapterPosition() , Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context,SelectedSongActivity.class);
                intent.putExtra("song",mdata.get(myViewHolder.getAdapterPosition()));
                context.startActivity(intent);
            }
        });
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.authorName.setText(mdata.get(position).getAuthorName());
        holder.songName.setText(mdata.get(position).getSongName());
        holder.img.setImageResource(mdata.get(position).getImg());
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

   public static class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private TextView authorName;
        private TextView songName;
        private LinearLayout itemList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            itemList = (LinearLayout)itemView.findViewById(R.id.item_playlist_ID);
            img = itemView.findViewById(R.id.itemImage_ID);
            authorName = itemView.findViewById(R.id.authorName_ID);
            songName = itemView.findViewById(R.id.songName_ID);

        }
    }
}
