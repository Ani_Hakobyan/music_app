package ani.am.music.activites;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import ani.am.music.R;
import ani.am.music.classes.Song;

public class SelectedSongActivity extends AppCompatActivity implements View.OnClickListener {
    private MediaPlayer player;
    private Button btnPlay;
    private SeekBar positionBar;
    private SeekBar volumeBar;
    private TextView elapsedTime;
    private TextView remainingTime;
    private TextView songName;
    private TextView textVolume;
    private ImageView songImage;
    int totalTime;

    Drawable imgPlay ;
    Drawable imgPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_song);

        Resources res = getResources();
        imgPlay = res.getDrawable(android.R.drawable.ic_media_play);
        imgPause = res.getDrawable(android.R.drawable.ic_media_pause);


        receiveSong();
        elapsedTime = (TextView)findViewById(R.id.elapsedTime);
        remainingTime = (TextView)findViewById(R.id.remainingTime);
        textVolume = (TextView)findViewById(R.id.textVolume);
        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(this);

        // MediaPlayer
        //player = MediaPlayer.create(this,R.raw.theme_from_love_story_piano);
        player.seekTo(0);
        player.setLooping(true);
        player.setVolume(0.5f,0.5f);
        totalTime = player.getDuration();

        //Position Bar
        positionBar = (SeekBar)findViewById(R.id.positionBar);
        positionBar.setMax(totalTime);
        positionBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    player.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //Volume Bar
        volumeBar = (SeekBar)findViewById(R.id.volumeBar);
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float volumeNum = progress/100f;
                player.setVolume(volumeNum,volumeNum);
                textVolume.setText(String.valueOf((int)(volumeNum*100)));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        playCicle();
    }
    public void receiveSong() {
        Song song = (Song) getIntent().getSerializableExtra("song");
        songName = (TextView) findViewById(R.id.tvName);
        songImage = (ImageView) findViewById(R.id.songName_ID);

        songName.setText(song.getAuthorName() + " " + song.getSongName());
        songImage.setImageResource(song.getImg());
        player = MediaPlayer.create(this, song.getPlayer());
    }
    @Override
    public void onClick(View v) {
        if(player.isPlaying()){
            //Stopping
            player.stop();
            btnPlay.setBackground(imgPlay);

        }else {
            //Playing
            player.start();
            btnPlay.setBackground(imgPause);
        }
    }
    public void playCicle(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(player!=null){
                    try {
                        Message msg = new Message();
                        msg.what = player.getCurrentPosition();
                        handler.sendMessage(msg);
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            int currentPosition = msg.what;
            //Update positionBar
            positionBar.setProgress(currentPosition);
            //Update Labels
            elapsedTime.setText(createTimeLabel(player.getCurrentPosition()));
            remainingTime.setText("-"+createTimeLabel(totalTime - player.getCurrentPosition()));

        }
    };

    private String createTimeLabel(int time) {
        String timeLabel = "";
        int min = time/1000/60;
        int sec = time/1000%60;
        timeLabel = min +":";
        if(sec < 10)
            timeLabel += "0";
        timeLabel += sec;

        return timeLabel;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.stop();
    }
}
