package ani.am.music.activites;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ani.am.music.R;
import ani.am.music.adapters.ViewPagerAdapter;
import ani.am.music.fragments.FavFragment;
import ani.am.music.fragments.PlaylistFragment;
import ani.am.music.fragments.SearchFragment;

public class MainActivity extends AppCompatActivity  {
     private TabLayout tabLayout;
     private ViewPager viewPager;
     private ViewPagerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = (TabLayout)findViewById(R.id.tabLayout_ID);
        viewPager = (ViewPager)findViewById(R.id.viewPager_ID);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        // Add fragments
        adapter.addFragment(new SearchFragment(),"");
        adapter.addFragment(new PlaylistFragment(),"");
        adapter.addFragment(new FavFragment(),"");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_search);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_playlist);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_favorite);


//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setElevation(0);

    }
}
